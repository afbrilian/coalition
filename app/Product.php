<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  protected $casts = [
      'the_field' => 'array',
  ];

  protected $fillable = [
  'name', 'qty', 'price', 'created_at'
  ];

  protected $table = 'product';
}

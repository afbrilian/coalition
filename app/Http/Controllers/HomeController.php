<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Product;

class HomeController extends Controller
{

  public function __construct() {}

  public function index()
  {
    $path = storage_path() . "/json/products.json";
    $file = file_exists($path);
    if (!$file) \File::put($path, json_encode([]));
    $products = collect(json_decode(file_get_contents($path)))
    ->sortByDesc(function ($item, $key) {
      return $item->created_at;
    });
    return view('index', ['products' => $products]);
  }

  public function save(Request $request)
  {
    $product = new Product;
    $product->name = $request->name;
    $product->qty = $request->qty;
    $product->price = $request->price;
    $product->total_value_number = $product->qty * $product->price;
    $product->created_at = new \DateTime();

    $path = storage_path() . "/json/products.json";
    $products = collect(json_decode(file_get_contents($path)));

    $pos = $this->findByName($products, $request->name);
    if($pos != false) return response()->json(['status' => 210, 'message' => 'Product with the same name already exist'], 200);

    $products->push($product->toArray());
    \File::put($path, json_encode($products));

    return response()->json(['status' => 209, 'product' => $product], 200);
  }

  public function edit(Request $request) {
    $path = storage_path() . "/json/products.json";
    $products = collect(json_decode(file_get_contents($path)));
    $pos = $this->findByName($products, $request->name);

    $product = $products[$pos];
    $product->name = $request->name;
    $product->qty = $request->qty;
    $product->price = $request->price;
    $product->total_value_number = $product->qty * $product->price;

    $products->splice($pos, 1, [$product]);
    \File::put($path, json_encode($products));

    return response()->json(['status' => 209, 'product' => $product], 200);
  }

  private function findByName($products, $name) {
    $pos = $products->search(function ($item, $key) use ($name) {
      return strcasecmp ($item->name, $name) == 0;
    });
    return $pos;
  }

}

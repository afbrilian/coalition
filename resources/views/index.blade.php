<!doctype html>
<html>
<head>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">
</head>
<body>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-sm-8">
        <!-- action="{{ route('save') }}" -->
        <form id="form-product" class="form-horizontal">

          <div class="form-group">
            <label for="inputProductName" class="col-sm-2 control-label">Product Name</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="inputProductName" placeholder="Product Name" name="name"
              @isset($data->name)
              value="{{ $data->name }}"
              @endisset
              required>
            </div>
          </div>

          <div class="form-group">
            <label for="inputQty" class="col-sm-2 control-label">Quantity</label>
            <div class="col-sm-10">
              <input type="number" class="form-control" id="inputQty" placeholder="Quantity" name="qty"
              @isset($data->qty)
              value="{{ $data->qty }}"
              @endisset
              required>
            </div>
          </div>

          <div class="form-group">
            <label for="inputPrice" class="col-sm-2 control-label">Price</label>
            <div class="col-sm-10">
              <input type="number" class="form-control" id="inputPrice" placeholder="Price" name="price"
              @isset($data->price)
              value="{{ $data->price }}"
              @endisset
              required>
            </div>
          </div>

          <button type="submit" class="btn btn-info pull-right">Submit</button>

        </form>
      </div>

      <div class="col-sm-12">
        <table id="product-table" class="table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Quantity</th>
              <th>Price</th>
              <th>Created At</th>
              <th>Total Value Number</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($products as $product)
            <tr>
              <td>{{ $product->name }}</td>
              <td>{{ $product->qty }}</td>
              <td>{{ $product->price }}</td>
              <td>{{ $product->created_at }}</td>
              <td>{{ $product->total_value_number }}</td>
              <td>
                <a href="#" class="initial-btn label label-info" onCLick="editProduct(this)" data-name="{!! $product->name !!}"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
              </td>
            </tr>
            @endforeach
            <tr>
              <td><b>Total</b></td>
              <td></td>
              <td></td>
              <td></td>
              <td>sum</td>
              <td></td>
            </tr>
          </tbody>
        </table>
      </div>
    </section>

  </body>
  <script src="{{ URL::asset('js/jquery.min.js') }}"></script>
  <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
  <script src="{{ URL::asset('js/app.js') }}"></script>
  </html>

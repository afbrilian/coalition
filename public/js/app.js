$(document).ready(function() {

  totalValueNumber();

  $('#form-product').on('submit', function(e) {
    e.preventDefault();

    var formData = {
      name: $('#inputProductName').val(),
      qty: $('#inputQty').val(),
      price: $('#inputPrice').val()
    }

    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      type: 'POST',
      url: 'save',
      data: formData,
      dataType: 'json',
      success: function(data, status, xhr) {
        if(xhr.status == 200) {
          if (data.status == 210) {
            alert(data.message);
          } else {
            alert('Success!');
            var resp = '<tr>';
            resp += '<td>' + data.product.name + '</td>';
            resp += '<td>' + data.product.qty + '</td>';
            resp += '<td>' + data.product.price + '</td>';
            resp += '<td>' + data.product.created_at + '</td>';
            resp += '<td>' + data.product.total_value_number + '</td>';
            resp += '<td>'
            + '<a href="#" class="label label-info" onCLick="editProduct(this)" data-name="' + data.product.name + '"><i class="glyphicon glyphicon-pencil"></i> Edit</a>'
            + '</td>';
            resp += '</tr>';
            if( $('#product-table > tbody > tr').length == 0 )
              $('#product-table > tbody:last-child').append(resp);
            else $('#product-table > tbody > tr:first').before(resp);

            $('#inputProductName').val(''),
            $('#inputQty').val(''),
            $('#inputPrice').val('')
            totalValueNumber();
          }
        } else alert('please try again with different values')
      },
      error: function (msg) {
        alert("Error: check console");
        console.log(msg);
      }
    });
});

});

function editProduct(ethis) {
  var name = $(ethis).data('name');
  var tdparent = $(ethis).parent();
  tdparent.find('.initial-btn').hide();

  tdparent.prepend('<a href="#" class="edit-btn label label-warning" onCLick="cancelEditProduct(this)" data-name="'+name+'"><i class="glyphicon glyphicon-remove"></i> Cancel</a>');
  tdparent.prepend('<a href="#" class="edit-btn label label-info" onCLick="saveEditProduct(this)" data-name="'+name+'"><i class="glyphicon glyphicon-ok"></i> Save</a>');

  var tr = $(ethis).parent().parent();
  for (var i=0; i < 3; i++) {
    var td = tr.find('td').eq(i);
    var initValue = td.text();
    var inp = '<input type="text" value="'+initValue+'"/><span class="hidden">'+initValue+'</span>';
    td.html(inp);
  }
}

function saveEditProduct(ethis) {
  var name = $(ethis).data('name');
  var tdparent = $(ethis).parent();
  var formData = {};
  var tr = $(ethis).parent().parent();

  var newname = tr.find('td').eq(0).find('input').val();
  var newqty = tr.find('td').eq(1).find('input').val();
  var newprice = tr.find('td').eq(2).find('input').val();

  var formData = {
    name: newname,
    qty: newqty,
    price: newprice
  }

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  $.ajax({
    type: 'POST',
    url: 'edit',
    data: formData,
    dataType: 'json',
    success: function(data, status, xhr) {
      if(xhr.status == 200) {
        if (data.status == 210) {
          alert(data.message);
        } else {
          alert('Success!');

          tdparent.find('.edit-btn').hide();
          tdparent.find('.initial-btn').show();

          tr.find('td').eq(0).html(data.product.name);
          tr.find('td').eq(1).html(data.product.qty);
          tr.find('td').eq(2).html(data.product.price);
          tr.find('td').eq(4).html(data.product.total_value_number);
          tr.find('td').eq(5).find('a').each(function() {
            $(this).attr('data-name', data.product.name);
          });
          totalValueNumber();
        }
      } else alert('please try again with different values')
    },
    error: function (msg) {
      alert("Error: check console");
      console.log(msg);
    }
  });
}

function cancelEditProduct(ethis) {
  var name = $(ethis).data('name');
  var tdparent = $(ethis).parent();
  tdparent.find('.edit-btn').hide();
  tdparent.find('.initial-btn').show();

  var tr = $(ethis).parent().parent();
  for (var i=0; i < 3; i++) {
    var td = tr.find('td').eq(i);
    var initValue = td.find('span').text();
    var inp = initValue;
    td.html(inp);
  }
}

function totalValueNumber() {
  var sum = 0;
  $('#product-table tbody').find('tr').not(':last').each(function() {
    sum += Number($(this).find('td').eq(4).html());
  });
  $('#product-table tr:last').find('td').eq(4).html(sum);
}
